\ProvidesClass{ScientificPaperDHBW} [2017/09/21 v1.00 Scientific Paper for the DHBW Horb]
\NeedsTeXFormat{LaTeX2e}

%%%%%%%%%%%%% Option Commands %%%%%%%%%%%%%%
\newcommand{\headStyle}{\pagestyle{plain}}
\newcommand{\headNumbering}{\pagenumbering{Roman}}
\newcommand{\textStyle}{\pagestyle{headings}}
\newcommand{\textNumbering}{\pagenumbering{arabic}}
%%%%%%%%%%%%% Options %%%%%%%%%%%%%%
\DeclareOption{headStyle=plain}{\renewcommand{\headStyle}{\pagestyle{plain}}}
\DeclareOption{headStyle=headings}{\renewcommand{\headStyle}{\pagestyle{headings}}}
\DeclareOption{headStyle=empty}{\renewcommand{\headStyle}{\pagestyle{empty}}}
\DeclareOption{headNumbering=Roman}{\renewcommand{\headNumbering}{\pagenumbering{Roman}}}
\DeclareOption{headNumbering=arabic}{\renewcommand{\headNumbering}{\pagenumbering{arabic}}}
\DeclareOption{textStyle=headings}{\renewcommand{\textStyle}{\pagestyle{headings}}}
\DeclareOption{textStyle=plain}{\renewcommand{\textStyle}{\pagestyle{plain}}}
\DeclareOption{textStyle=empty}{\renewcommand{\textStyle}{\pagestyle{empty}}}
\DeclareOption{textNumbering=arabic}{\renewcommand{\textNumbering}{\pagenumbering{arabic}}}
\DeclareOption{textNumbering=Roman}{\renewcommand{\textNumbering}{\pagenumbering{Roman}}}

\DeclareOption*{\InputIfFileExists{\CurrentOption.min}{}{%
	\PassOptionsToClass{\CurrentOption}{scrbook}}}
% --- Class structure: execution of options part
% ---
\ProcessOptions \relax
% --- Class structure: declaration of options part
% ---
\LoadClass[oneside, headsepline, footsepline, toc=bibliography, chapterprefix]{scrbook}
%\LoadClass[%
%	pdftex,
%	oneside,			% Zweiseitiger Druck.
%	11pt,				% Schriftgroesse
%	parskip=half,		% Halbe Zeile Abstand zwischen Absätzen.
%	topmargin = 10pt,	% Abstand Seitenrand (Std:1in) zu Kopfzeile [laut log: unused]
%	headheight = 12pt,	% Höhe der Kopfzeile
%	headsep = 30pt,	% Abstand zwischen Kopfzeile und Text Body  [laut log: unused]
%	headsepline,		% Linie nach Kopfzeile.
%	footsepline,		% Linie vor Fusszeile.
%	footheight = 16pt,	% Höhe der Fusszeile
%	abstracton,		% Abstract Überschriften
%	DIV=calc,		% Satzspiegel berechnen
%	BCOR=8mm,		% Bindekorrektur links: 8mm
%	headinclude=false,	% Kopfzeile nicht in den Satzspiegel einbeziehen
%	footinclude=false,	% Fußzeile nicht in den Satzspiegel einbeziehen
%	listof=totoc,		% Abbildungs-/ Tabellenverzeichnis im Inhaltsverzeichnis darstellen
%	toc=bibliography,	% Literaturverzeichnis im Inhaltsverzeichnis darstellen
%]{scrbook}

%%%%%%%%%% Packeges %%%%%%%%%%
%Page layout
\RequirePackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2.5cm,includeheadfoot]{geometry}
%Header and footer
\RequirePackage{longtable}
\RequirePackage{graphicx}
\RequirePackage[utf8]{inputenc}
\RequirePackage{listings}
\RequirePackage{xcolor}
%\RequirePackage{times}
\RequirePackage{ifthen}
\RequirePackage{setspace}
\RequirePackage{csquotes}
\RequirePackage[backend=bibtex,bibencoding=ascii]{biblatex}
%\RequirePackage{lmodern}
%%%%%%%%%% Booleans %%%%%%%
\newboolean{alreadyEnded}
%%%%%%%%%% Data %%%%%%%%%%
%Autor
\def\autor#1{\def\@autor{#1}}
\newcommand{\Autor}{\@autor}
%Firma
\def\firma#1{\def\@firma{#1}}
\newcommand{\Firma}{\@firma}
%FirmenOrt
\def\firmenort#1{\def\@firmenort{#1}}
\newcommand{\Firmenort}{\@firmenort}
%AbgabeOrt
\def\abgabeort#1{\def\@abgabeort{#1}}
\newcommand{\Abgabeort}{\@abgabeort}
%Abschluss
\def\abschluss#1{\def\@abschluss{#1}}
\newcommand{\Abschluss}{\@abschluss}
%Title
\def\title#1{\def\@title{#1}}
\newcommand{\Title}{\@title}
%Kurs
\def\kurs#1{\def\@kurs{#1}}
\newcommand{\Kurs}{\@kurs}
%Studiengang
\def\studiengang#1{\def\@studiengang{#1}}
\newcommand{\Studiengang}{\@studiengang}
%Hochschule
\def\hochschule#1{\def\@hochschule{#1}}
\newcommand{\Hochschule}{\@hochschule}
%Betreuer
\def\betreuer#1{\def\@betreuer{#1}}
\newcommand{\Betreuer}{\@betreuer}
%BetreuerAbschluss
\def\betreuerabschluss#1{\def\@betreuerabschluss{#1}}
\newcommand{\Betreuerabschluss}{\@betreuerabschluss}
%Gutachter
\def\gutachter#1{\def\@gutachter{#1}}
\newcommand{\Gutachter}{\@gutachter}
%Gutachterabschluss
\def\gutachterabschluss#1{\def\@gutachterabschluss{#1}}
\newcommand{\Gutachterabschluss}{\@gutachterabschluss}
%Zeitraum
\def\zeitraum#1{\def\@zeitraum{#1}}
\newcommand{\Zeitraum}{\@zeitraum}
%Arbeit
\def\arbeit#1{\def\@arbeit{#1}}
\newcommand{\Arbeit}{\@arbeit}
%MartrikelNR
\def\matNR#1{\def\@matNR{#1}}
\newcommand{\MatNR}{\@matNR}
%AbgabeDatum
\def\abgabeDatum#1{\def\@abgabeDatum{#1}}
\newcommand{\AbgabeDatum}{\@abgabeDatum}

%%%%%%%%%% New Commands %%%%%%%%%%
\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here
%%%%%%%%%% Title %%%%%%%%%%
\renewcommand{\maketitle}{
\renewcommand{\baselinestretch}{1.25}

	\begin{titlepage}
	\center
		%----------------------------------------------------------------------------------------
		%   Logos
		%----------------------------------------------------------------------------------------

		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \normalsize
				\includegraphics[width = \textwidth]{images/firma.png}
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \normalsize
				\includegraphics[width = \textwidth]{images/hochschule.png}
			\end{flushright}
		\end{minipage}\\[1cm]

		%----------------------------------------------------------------------------------------
		%   TITLE SECTION
		%----------------------------------------------------------------------------------------

		\HRule \\[0.4cm]
		{ \LARGE \bfseries \Title{} }\\[0.4cm] % Title of your document
		\HRule \\[1cm]

		%----------------------------------------------------------------------------------------
		%   HEADING SECTIONS
		%----------------------------------------------------------------------------------------

		\large \Arbeit\\ % Name of your university/college
		\normalsize \Abschluss\\ \vspace{1cm} % Major heading such as course name
		\normalsize Major in \Studiengang\\  % Minor heading such as course title
		\normalsize on the \Hochschule\\ \vspace{2cm}

		%----------------------------------------------------------------------------------------
		%   AUTHOR SECTION
		%----------------------------------------------------------------------------------------
		\begin{minipage}{0.4\textwidth}
			\begin{flushleft} \normalsize
				\emph{Author:}\\
				\Autor \\
				\vspace{0.5cm}
				\emph{Matriculation Number, Class:}\\
				\MatNR{}, \Kurs \\
				%\vspace{\baselineskip}
				\vspace{0.5cm}
				\emph{Complition Time:}\\
				\Zeitraum\\
				%\vspace{\baselineskip}
				%\vspace{\baselineskip}
			\end{flushleft}
		\end{minipage}
		~
		\begin{minipage}{0.4\textwidth}
			\begin{flushright} \normalsize
				\emph{Training Company:}\\
				\Firma, \Firmenort \\
				\vspace{0.5cm}
				\emph{Supervisor:} \\
				\Betreuer, \\ \Betreuerabschluss \\
				\vspace{0.5cm}
				\emph{Evaluator:}\\
				\Gutachter, \\ \Gutachterabschluss
			\end{flushright}
		\end{minipage}\\[1cm]

		%----------------------------------------------------------------------------------------
		%   DATE SECTION
		%----------------------------------------------------------------------------------------

		\normalsize \AbgabeDatum\\[2cm] % Date, change the \today to a set date if you want to be precise
		\vfill % Fill the rest of the page with whitespace


	\end{titlepage}
	\renewcommand{\baselinestretch}{1.5}

	\clearpage
	\headStyle
	\headNumbering
}

%%%%%%%%%% Erklärung %%%%%%%%%%
\newcommand{\headoptions}[2]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{#2}}{}
	\ifthenelse{\equal{#1}{toc,empty}}{\addchap{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{notoc,empty}}{\addchap*{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{empty,toc}}{\addchap{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{empty,notoc}}{\addchap*{#2} \thispagestyle{empty}}{}
	\ifthenelse{\equal{#1}{notoc}}{\addchap*{#2}}{}
	\ifthenelse{\equal{#1}{empty}}{\addchap*{#2} \thispagestyle{empty}}{}

}
\newcommand{\erklaerungDE}[1][notoc]{
	\headoptions{#1}{Erklärung}

	%\section*{Erklärung}
	\vspace*{2em}

	Ich erkläre hiermit ehrenwörtlich:
	\begin{enumerate}
		\item dass ich meine {\Arbeit} mit dem Thema {\itshape \Title } ohne fremde Hilfe angefertigt habe;
		\item dass ich die Übernahme wörtlicher Zitate aus der Literatur sowie die Verwendung der Gedanken anderer Autoren an den entsprechenden Stellen innerhalb der Arbeit gekennzeichnet habe;
	\end{enumerate}
	Ich bin mir bewusst, dass eine falsche Erklärung rechtliche Folgen haben wird.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
\newcommand{\erklaerungEN}[1][notoc]{
	\headoptions{#1}{Declaration of Authorship}
	%\section*{Declaration of Authorship }
	\vspace*{2em}
	Hereby I solemnly declare:
	\begin{enumerate}
		\item that this \Arbeit{}, titled \textit{\Title} is entirely the product of my own scholarly work, unless otherwise indicated in the text or references, or acknowledged below;
		\item I have indicated the thoughts adopted directly or indirectly from other sources at the appropriate places within the document;
		\item this \Arbeit{} has not been submitted either in whole or part, for a degree at this or any other university or institution;
		\item I have not published this \Arbeit{} in the past;
		\item the printed version is equivalent to the submitted electronic one.
	\end{enumerate}
	\vspace{3em}
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
%%%%%%%%%% Sperrvermerk %%%%%%%%%%
\newcommand{\sperrvermerkDE}[1][notoc]{
	\headoptions{#1}{Sperrvermerk}
	%\section*{Sperrvermerk}
	\vspace*{2em}
	Die vorliegende {\Arbeit{}} mit dem Titel {\itshape{} \Title{}\/} enthält unternehmensinterne bzw. vertrauliche Informationen der {\Firma}, ist deshalb mit einem Sperrvermerk versehen und wird ausschließlich zu Prüfungszwecken am Studiengang {\Studiengang} der \Hochschule vorgelegt. Sie ist ausschließlich zur Einsicht durch den zugeteilten Gutachter, die Leitung des Studiengangs und ggf. den Prüfungsausschuss des Studiengangs bestimmt.  Es ist untersagt,
	\begin{itemize}
		\item den Inhalt dieser Arbeit (einschließlich Daten, Abbildungen, Tabellen, Zeichnungen usw.) als Ganzes oder auszugsweise weiterzugeben,
		\item Kopien oder Abschriften dieser Arbeit (einschließlich Daten, Abbildungen, Tabellen, Zeichnungen usw.) als Ganzes oder in Auszügen anzufertigen,
		\item diese Arbeit zu veröffentlichen bzw. digital, elektronisch oder virtuell zur Verfügung zu stellen.
	\end{itemize}
	Jede anderweitige Einsichtnahme und Veröffentlichung – auch von Teilen der Arbeit – bedarf der vorherigen Zustimmung durch den Verfasser und {\Firma}.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}
\newcommand{\sperrvermerkEN}[1][notoc]{
	\headoptions{#1}{Confidentiality clause}

	\ifthenelse{\equal{#1}{toc}}{\addchap{Confidentiality clause}}{\addchap*{Confidentiality clause}}
	%\section*{Confidentiality clause}
	\vspace*{2em}
	The {\Arbeit} on hand {\itshape{} \Title{}\/}contains internal resp.\ confidential data of {\Firma}. It is intended solely for inspection by the assigned examiner, the head of the {\Studiengang} department and, if necessary, the Audit Committee \Hochschule. It is strictly forbidden
	\begin{itemize}
		\item to distribute the content of this paper (including data, figures, tables, charts etc.) as a whole or in extracts,
		\item to make copies or transcripts of this paper or of parts of it,
		\item to display this paper or make it available in digital, electronic or virtual form.
	\end{itemize}
	Exceptional cases may be considered through permission granted in written form by the author and {\Firma}.
	\vspace{3em}\\
	\Abgabeort, \AbgabeDatum
	\vspace{4em}\\
	\rule{6cm}{0.4pt}\\
	\Autor
	\clearpage
}

%%%%%%%%%%%% List of tables %%%%%%
\let\oldlistoftables\listoftables
\renewcommand{\listoftables}[1][toc]{
	\cleardoublepage
	\ifthenelse{\equal{#1}{toc}}{\addcontentsline{toc}{chapter}{List of Tables}}{}
	\oldlistoftables
	\clearpage
}

%%%%%%%%% List of figures %%%%
\let\oldlistoffigures\listoffigures
\renewcommand{\listoffigures}[1][toc]{
	\cleardoublepage
	\ifthenelse{\equal{#1}{toc}}{\addcontentsline{toc}{chapter}{List of Figures}}{}
	\oldlistoffigures
	\clearpage
}
%%%%%%%%%% Abstract %%%%%%%%%%
\newcommand{\abstractDE}[2][toc]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{Zusammenfassung}}{\addchap*{Zusammenfassung}}
	#2
	\clearpage
}
\newcommand{\abstractEN}[2][toc]{
	\ifthenelse{\equal{#1}{toc}}{\addchap{Abstract}}{\addchap*{Abstract}}
	\begin{otherlanguage}{english}
		#2
	\end{otherlanguage}
	\clearpage
}
%%%%%%%%%% Table of content %%%%%%%%%%%
\let\oldtableofcontents\tableofcontents
\renewcommand{\tableofcontents}{
	\clearpage
	\begin{spacing}{1.1}
		\begingroup

		% auskommentieren für Seitenzahlen unter Inhaltsverzeichnis
		\renewcommand*{\chapterpagestyle}{empty}
		\pagestyle{empty}


		\setcounter{tocdepth}{2}
		%für die Anzeige von Unterkapiteln im Inhaltsverzeichnis
		%\setcounter{tocdepth}{2}

		\oldtableofcontents
		\clearpage
		\endgroup
	\end{spacing}
	%	\pagenumbering{gobble}
	%	\pagestyle{empty}
	%	\setcounter{tocdepth}{2}
	%	\oldtableofcontents
	\clearpage
	\textStyle
	\textNumbering
	%\setboolean{alreadyEnded}{true}
}
%%%%%%%%%% Past all Content %%%%%%%%%
\newcommand{\content}[2]{
	\foreach \i in {01,02,03,04,05,06,07,08,09,...,99} {
		\edef\FileName{#1/\i #2}
			\IfFileExists{\FileName}{
				\input{\FileName}
			}
			{
				%file does not exist
			}
		}
		\clearpage

}
%%%%%%%%%% Einstellungen %%%%%%%%%%
% Hurenkinder und Schusterjungen verhindern
% http://projekte.dante.de/DanteFAQ/Silbentrennung
\clubpenalty = 10000 % schließt Schusterjungen aus (Seitenumbruch nach der ersten Zeile eines neuen Absatzes)
\widowpenalty = 10000 % schließt Hurenkinder aus (die letzte Zeile eines Absatzes steht auf einer neuen Seite)
\displaywidowpenalty=10000
\setstretch{1,25}
\headStyle
\headNumbering
\defbibheading{head}{\addchap{Literaturverzeichnis}}

\addtokomafont{chapter}{\rmfamily}
\addtokomafont{section}{\rmfamily}
\addtokomafont{subsection}{\rmfamily}
\addtokomafont{chapterentry}{\rmfamily}
\addtokomafont{author}{\rmfamily}
\addtokomafont{caption}{\rmfamily}
\addtokomafont{captionlabel}{\rmfamily}
\addtokomafont{chapterentrydots}{\rmfamily}
\addtokomafont{chapterentrypagenumber}{\rmfamily}
\addtokomafont{chapterprefix}{\rmfamily}
\addtokomafont{date}{\rmfamily}
\addtokomafont{dedication}{\rmfamily}
\addtokomafont{dictum}{\rmfamily}
\addtokomafont{dictumtext}{\rmfamily}
\addtokomafont{disposition}{\rmfamily}
\addtokomafont{footnote}{\rmfamily}
\addtokomafont{footnotelabel}{\rmfamily}
\addtokomafont{footnotereference}{\rmfamily}
\addtokomafont{footnoterule}{\rmfamily}
\addtokomafont{labelinglabel}{\rmfamily}
\addtokomafont{labelingseparator}{\rmfamily}
\addtokomafont{minisec}{\rmfamily}
\addtokomafont{pagehead}{\rmfamily}
\addtokomafont{pagefoot}{\rmfamily}
\addtokomafont{pageheadfoot}{\rmfamily}
\addtokomafont{pagenumber}{\rmfamily}
\addtokomafont{pagination}{\rmfamily}
\addtokomafont{paragraph}{\rmfamily}
\addtokomafont{part}{\rmfamily}
\addtokomafont{partentry}{\rmfamily}
\addtokomafont{partentrypagenumber}{\rmfamily}
\addtokomafont{partnumber}{\rmfamily}
\addtokomafont{sectioning}{\rmfamily}
\addtokomafont{subject}{\rmfamily}
\addtokomafont{subparagraph}{\rmfamily}
\addtokomafont{subsection}{\rmfamily}
\addtokomafont{subsubsection}{\rmfamily}
\addtokomafont{subtitle}{\rmfamily}
\addtokomafont{title}{\rmfamily}
\addtokomafont{titlehead}{\rmfamily}
\addtokomafont{descriptionlabel}{\rmfamily}

%%% REST DOC

%% caption less table to specify restful primitives
\newcommand{\restful}[1]
{
\begin{center}
\begin{tabular}{l p{12cm}}
\hline
#1
\hline
\end{tabular}
\end{center}
\vspace{6pt}
}


%% caption less table to specify restful uri
\newcommand{\routes}[1]
{
\begin{center}
\begin{tabular}{p{\textwidth}}
\hline
#1
\hline
\end{tabular}
\end{center}
\vspace{6pt}
}
%
%
%

\newcommand{\mimetype}[2]{#1 & #2 \\\noalign{\smallskip}}
\newcommand{\head}[2]{#1 & #2 \\\noalign{\smallskip}}
\newcommand{\uri}[2]{\url{#1} \\ #2 \vspace{8pt} \\\noalign{\smallskip}}
\newcommand{\env}[2]{#1 & #2 \\\noalign{\smallskip}}
%

%%% resource definition table
\newcommand{\resource}[1]
{
\begin{center}
\begin{tabular}{l l p{12cm}}
\hline
#1
\hline
\end{tabular}
\end{center}
\vspace{6pt}
}
\newcommand{\attr}[3]{{\tt #1} & #2 & #3 \\\noalign{\smallskip}}

%
%
\newcommand\Tstrut{\rule{0pt}{2.6ex}}         % = `top' strut
\newcommand\Bstrut{\rule[-0.9ex]{0pt}{0pt}}   % = `bottom' strut
\newcommand{\request}[5]
{
\begin{center}
\begin{tabular}{l p{12cm}}
\hline
\multicolumn{2}{l}{\textbf{#1}} \Tstrut\Bstrut\\
\hline
\textbf{Description} & #2 \vspace{4pt} \Tstrut\\

\textbf{Parameters} & #3 \vspace{4pt}\\

\textbf{Responses} & #4 \vspace{4pt}\\

\textbf{Status code} & #5 \Bstrut\\
\hline
\end{tabular}
\end{center}
\vspace{6pt}
}

\newcommand{\sep}{\\\noalign{\smallskip} &}

\newcommand{\status}[1]
{
\ifthenelse{\equal{#1}{100}}{100 Continue}{}%
\ifthenelse{\equal{#1}{101}}{101 Switching Protocols}{}%
\ifthenelse{\equal{#1}{200}}{200 OK}{}%
\ifthenelse{\equal{#1}{201}}{201 Created}{}%
\ifthenelse{\equal{#1}{202}}{202 Accepted}{}%
\ifthenelse{\equal{#1}{203}}{203 Non-Authoritative Information}{}%
\ifthenelse{\equal{#1}{204}}{204 No Content}{}%
\ifthenelse{\equal{#1}{205}}{205 Reset Content}{}%
\ifthenelse{\equal{#1}{206}}{206 Partial Content}{}%
\ifthenelse{\equal{#1}{300}}{300 Multiple Choices}{}%
\ifthenelse{\equal{#1}{301}}{301 Moved Permanently}{}%
\ifthenelse{\equal{#1}{302}}{302 Found}{}%
\ifthenelse{\equal{#1}{303}}{303 See Other}{}%
\ifthenelse{\equal{#1}{304}}{304 Not Modified}{}%
\ifthenelse{\equal{#1}{307}}{307 Temporary Redirect}{}%
\ifthenelse{\equal{#1}{302}}{302 Found}{}%
\ifthenelse{\equal{#1}{400}}{400 Bad Request}{}%
\ifthenelse{\equal{#1}{401}}{401 Unauthorized}{}%
\ifthenelse{\equal{#1}{402}}{402 Payment Required}{}%
\ifthenelse{\equal{#1}{403}}{403 Forbidden}{}%
\ifthenelse{\equal{#1}{404}}{404 Not Found}{}%
\ifthenelse{\equal{#1}{405}}{405 Method Not Allowed}{}%
\ifthenelse{\equal{#1}{406}}{406 Not Acceptable}{}%
\ifthenelse{\equal{#1}{407}}{407 Proxy Authentication Required}{}%
\ifthenelse{\equal{#1}{408}}{408 Request Timeout}{}%
\ifthenelse{\equal{#1}{409}}{409 Conflict}{}%
\ifthenelse{\equal{#1}{410}}{410 Gone}{}%
\ifthenelse{\equal{#1}{411}}{411 Length Required}{}%
\ifthenelse{\equal{#1}{412}}{412 Precondition Failed}{}%
\ifthenelse{\equal{#1}{413}}{413 Request Entity Too Large}{}%
\ifthenelse{\equal{#1}{414}}{414 Request-URI Too Long}{}%
\ifthenelse{\equal{#1}{415}}{415 Unsupported Media Type}{}%
\ifthenelse{\equal{#1}{416}}{416 Requested Range Not Satisfiable}{}%
\ifthenelse{\equal{#1}{417}}{417 Expectation Failed}{}%
\ifthenelse{\equal{#1}{422}}{422 Unprocessable Entity}{}%
\ifthenelse{\equal{#1}{500}}{500 Internal Server Error}{}%
\ifthenelse{\equal{#1}{501}}{501 Not Implemented}{}%
\ifthenelse{\equal{#1}{502}}{502 Bad Gateway}{}%
\ifthenelse{\equal{#1}{503}}{503 Service Unavailable}{}%
\ifthenelse{\equal{#1}{504}}{504 Gateway Timeout}{}%
\ifthenelse{\equal{#1}{505}}{505 HTTP Version Not Supported}{}%
}
\colorlet{punct}{red!60!black}
\definecolor{background}{HTML}{EEEEEE}
\definecolor{delim}{RGB}{20,105,176}
\colorlet{numb}{magenta!60!black}

\lstdefinelanguage{json}{
    basicstyle=\normalfont\ttfamily,
    numberstyle=\scriptsize,
    showstringspaces=false,
    breaklines=true,
    literate=
     *{0}{{{\color{numb}0}}}{1}
      {1}{{{\color{numb}1}}}{1}
      {2}{{{\color{numb}2}}}{1}
      {3}{{{\color{numb}3}}}{1}
      {4}{{{\color{numb}4}}}{1}
      {5}{{{\color{numb}5}}}{1}
      {6}{{{\color{numb}6}}}{1}
      {7}{{{\color{numb}7}}}{1}
      {8}{{{\color{numb}8}}}{1}
      {9}{{{\color{numb}9}}}{1}
      {:}{{{\color{punct}{:}}}}{1}
      {,}{{{\color{punct}{,}}}}{1}
      {\{}{{{\color{delim}{\{}}}}{1}
      {\}}{{{\color{delim}{\}}}}}{1}
      {[}{{{\color{delim}{[}}}}{1}
      {]}{{{\color{delim}{]}}}}{1},
}

\newcommand{\httpcode}[2]{\status{#1} & #2 \\\noalign{\smallskip}}

\newcommand{\example}[1]{\noindent {\bf #1}}
